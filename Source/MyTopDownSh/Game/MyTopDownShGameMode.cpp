// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTopDownShGameMode.h"
#include "MyTopDownShPlayerController.h"
//#include "MyTopDownSh/Character/MyTopDownShCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTopDownShGameMode::AMyTopDownShGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTopDownShPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}