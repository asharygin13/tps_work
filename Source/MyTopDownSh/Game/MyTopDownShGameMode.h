// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyTopDownShGameMode.generated.h"

UCLASS(minimalapi)
class AMyTopDownShGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyTopDownShGameMode();
};



