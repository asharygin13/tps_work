// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTopDownSh.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MyTopDownSh, "MyTopDownSh" );

DEFINE_LOG_CATEGORY(LogMyTopDownSh)
 